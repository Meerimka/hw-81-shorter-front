import axios from '../axios-api';

export const CREATE_LINKS_SUCCESS ='CREATE_LINKS_SUCCESS';
export const createLinksSuccess = (data) =>({type: CREATE_LINKS_SUCCESS, data});


export const sendLinks = data =>{
    return dispatch =>{
        return axios.post('/short', data).then(
            response=> {
                return dispatch(createLinksSuccess(response.data))}
        );
    };
};



