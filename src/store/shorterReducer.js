import {CREATE_LINKS_SUCCESS} from "./shorterActions";

const initialState ={
    shorterLink: ''
};

const shorterReducer = (state=initialState, action)=>{
    switch (action.type) {
        case CREATE_LINKS_SUCCESS:
            return{...state, shorterLink: action.data};
        default:
            return state;

    }
};

export default shorterReducer;