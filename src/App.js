import React, {Component, Fragment} from 'react';
import {Button, Container, Input, InputGroup, InputGroupAddon} from "reactstrap";
import {connect} from "react-redux";
import {sendLinks} from "./store/shorterActions";

class App extends Component {

    state={
        originalLink: ''
    };

    submitFormHandler = event => {
        event.preventDefault();
        this.props.sendLinks({...this.state})
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    render() {
    return (
        <Fragment>
            <Container style={ {paddingTop: "20px", textAlign: "center"}}>
                <h1>Shorten Your Link!</h1>
                <InputGroup >
                    <Input
                        type="text" required
                        name="originalLink" id="originalLink"
                        placeholder="Enter link"
                        value={this.state.originalLink}
                        onChange={this.inputChangeHandler}
                    />
                </InputGroup>
                <InputGroupAddon addonType="append">
                    <Button style={ {margin: "20px auto"}} onClick={this.submitFormHandler} color="primary">Shorter link </Button>
                </InputGroupAddon>
                {this.props.shortLink ?
                    <div style={ {marginTop: "20px"}}>
                        <p  >Your link now looks like this:</p>
                        <a href={'http://localhost:8000/short/'+this.props.shortLink}>{this.props.shortLink}</a>
                    </div>: null}
            </Container>
        </Fragment>
    );
  }
}
const mapStateToProps =(state)=>({
    shortLink: state.shorterLink
});

const  mapDispatchToProps= dispatch =>({
    sendLinks: data => dispatch(sendLinks(data))
});
export default connect(mapStateToProps,mapDispatchToProps)(App);
